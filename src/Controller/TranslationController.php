<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @Route("/translator")
 */
class TranslationController extends Controller
{
    /**
     * @Route("/translator", name="translator")
     */
    public function translator(Request $request, \Symfony\Component\Translation\TranslatorInterface $translator)
    {
        $body = $translator->trans('translated.token');
        $body .= "<br />";
        $body .= $translator->trans('i am translated text');
        $body .= "<br />";
        $body .= $translator->trans('product.is.very.good', ['%title%' => '36" Full HD Smart TV']);
        return new Response(sprintf("<html><body>%s</body> </html>", $body));
    }

    /**
     * @Route("/{_locale}/translator", name="translator_locale")
     */
    public function translatorLocale(Request $request, \Symfony\Component\Translation\TranslatorInterface $translator)
    {
        $body = $translator->trans('translated.token');
        $body .= "<br />";
        $body .= $translator->trans('i am translated text');
        $body .= "<br />";
        $body .= $translator->trans('product.is.very.good', ['%title%' => '36" Full HD Smart TV']);

        return new Response(sprintf("<html><body>%s</body> </html>", $body));
    }

    /**
     * @Route("/{_locale}/twig", name="twig")
     */
    public function twig(Request $request)
    {
        return $this->render("translations/translations.html.twig");
    }

    /**
     * @Route("/{_locale}/form", name="form")
     */
    public function form(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('name', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'label' => 'form.name',
                'constraints' => [
                    new NotBlank([ 'message' => 'not.blank.message' ])
                ]
            ])
            ->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                'label' => 'form.submit',
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            var_dump("form is valid!");
        }

        return $this->render("translations/form.html.twig", ['form' => $form->createView() ]);
    }
}
