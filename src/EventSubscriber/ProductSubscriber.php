<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\ProductEvent;
use Doctrine\ORM\EntityManagerInterface;

class ProductSubscriber implements EventSubscriberInterface
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onProductCreated(ProductEvent $event)
    {
        $product = $event->getProduct();
        // Realiam gyvenime darytume kažką įdomaus
        //$this->em->persist($product);

        // Bet čia darysim paprasčiau:
        var_dump("Will process:" . $product);
    }

    public static function getSubscribedEvents()
    {
        return [
           'product.created' => 'onProductCreated',
        ];
    }
}
