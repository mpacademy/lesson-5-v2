<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Article;

class CreatedAtSubscriber implements EventSubscriberInterface
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var Article $entity */
        $entity = $args->getEntity();

        if (!($entity instanceof Article)) {
            return;
        }

        $entity->setCreatedAt(new \DateTime());

        // !!! Negalima kviest persist/flush
    }

    public static function getSubscribedEvents()
    {
        return [
           'prePersist' => 'prePersist',
        ];
    }
}
