<?php

return [
    'translated.token' => 'Čia yra išverstas tokenas',
    'product.is.very.good' => 'Prekė <strong>%title%</strong> yra labai gera',
    'form.name' => 'Jūsų vardas',
    'form.submit' => 'Važiuojam',
];
