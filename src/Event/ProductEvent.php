<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

class ProductEvent extends Event
{
    private $product;
    
    /**
     * Get product.
     *
     * @return product.
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * Set product.
     *
     * @param product the value to set.
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
}
