<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Event\ProductEvent;

/**
 * @Route("/event")
 */
class EventController extends Controller
{
    /**
     * @Route("/request", name="event_request")
     */
    public function request(Request $request)
    {
        return $this->render('event/onRequest.html.twig');
    }

    /**
     * @Route("/custom", name="event_custom")
     */
    public function custom(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        $product = "Mano produktas";

        $productEvent = new ProductEvent();
        $productEvent->setProduct($product);
        $eventDispatcher->dispatch('product.created', $productEvent);

        return $this->render('event/custom.html.twig');
    }
}
